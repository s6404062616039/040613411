<?php include "db.php" ?>

<?php
$stmt = $pdo->prepare("UPDATE member SET username=?, mobile=?, email=? WHERE username=?"); // Prepare the query

$stmt->bindParam(1, $_POST["username"]); // Bind the parameters
$stmt->bindParam(2, $_POST["mobile"]);
$stmt->bindParam(3, $_POST["email"]);
$stmt->bindParam(4, $_POST["username"]); // Bind the fourth parameter for the WHERE clause

if ($stmt->execute()) {
    echo "แก้ไขสมาชิก " . $_POST["username"] . " สำเร็จ";
} else {
    // Handle the case where the query execution fails
    echo "เกิดข้อผิดพลาดในการแก้ไขข้อมูล";
}
?>
